# dotfiles

Dotfiles for my Debian- and Mac-based setups.
They're both relatively minimalistic configurations; as one German Scientist Man once said:

> Everything should be made as simple as possible, but not simpler.
>
> -- German Scientist Man
