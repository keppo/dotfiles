{:repl {:plugins [[cider/cider-nrepl "0.12.0"]
                  [refactor-nrepl "2.2.0"]]
        :dependencies [[alembic "0.3.2"]
                       [org.clojure/tools.nrepl "0.2.12"]]}}
